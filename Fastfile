# Customise this file, documentation can be found here:
# https://github.com/fastlane/fastlane/tree/master/fastlane/docs
# All available actions: https://docs.fastlane.tools/actions
# can also be listed using the `fastlane actions` command

# Change the syntax highlighting to Ruby
# All lines starting with a # are ignored when running `fastlane`

# If you want to automatically update fastlane if a new version is available:
# update_fastlane

# This is the minimum version number required.
# Update this, if you use features of a newer version
fastlane_version "2.28.3"

default_platform :ios

desc "Check fastlane env, update fastlane"
lane :check_env do
  if is_ci?
    puts "CI environment!"
  else
    say "Hi Human!"
  end
  sh "bundle update"
  update_fastlane
end

desc "Prepare app env"
lane :prepare_app do
  sh "cd .. && npm install"
end

desc "Units tests"
lane :unit_tests do
  # Define script test:unit:ci in your project like : karma start ./test/unit/config/karma.conf.js --coverage --single-run --browsers ChromeHeadless
  sh "cd .. && npm run test:unit:ci"
end

platform :ios do
  before_all do
    ENV["APP_ID"] = CredentialsManager::AppfileConfig.try_fetch_value(:app_identifier)
    ENV["APPLE_ID"] = CredentialsManager::AppfileConfig.try_fetch_value(:apple_id)
    ENV["TEAM_ID"] = CredentialsManager::AppfileConfig.try_fetch_value(:team_id)
    ENV["TEAM_NAME"] = CredentialsManager::AppfileConfig.try_fetch_value(:team_name)

    puts "Fastlane launched with following environment (IOS): "
    puts "Scheme : " + ENV["SCHEME"]
    puts "IOS workspace (directory) : " + ENV["WORKSPACE_IOS"]
    puts "IOS project (directory) : " + ENV["PROJECT_IOS"]
    puts "Application : "
    puts "\tID : " + ENV["APP_ID"]
    puts "\tName : " + ENV["APP_NAME"]
    puts "\tVersion : " + ENV["APP_VERSION"]
    puts "Apple Environment :"
    puts "\tApplication ID : " + ENV["APP_ID"]
    puts "\tApple ID : " + ENV["APPLE_ID"]
    puts "\tTeam ID : " + ENV["TEAM_ID"]
    puts "\tTeam name : " + ENV["TEAM_NAME"]
  end

  desc "Prepare IOS platform"
  lane :prepare_platform do
    sh "cd .. && npx ionic cordova platform add ios"
  end

  desc "Build IOS ionic"
  lane :build_ionic do
    sh "cd .. && npx ionic cordova build ios"
  end

  desc "Generate and sign debug ipa file"
  lane :sign_beta do
    prepare_ios_build_lanes(
        match_type: "development",
        code_sign_identity: "iPhone Developer",
        match_profile_type: "Development"
    )

    build_app(
      scheme: ENV["SCHEME"],
      workspace: ENV["WORKSPACE_IOS"],
      export_method: "development",
      export_options: {
        provisioningProfiles: {
          ENV["APP_ID"]=>"match Development " + ENV["APP_ID"]
        }
      },
      include_bitcode: true,
      clean: true
    )

  end

  desc "Generate and sign released ipa file"
  lane :sign_release do
    prepare_ios_build_lanes(
        match_type: "appstore",
        code_sign_identity: "iPhone Distribution",
        match_profile_type: "AppStore"
    )

    build_app(
      scheme: ENV["SCHEME"],
      workspace: ENV["WORKSPACE_IOS"],
      export_method: "app-store",
      include_bitcode: false,
      clean: true
    )
  end

  desc "Upload (testflight) released ipa file"
  lane :deploy_release do
    match(
      type: "appstore",
      readonly: is_ci
    ) # more information: https://codesigning.guide

    upload_to_testflight(
      skip_waiting_for_build_processing: true
    )
  end

  desc "Prepare IOS build lanes"
  private_lane :prepare_ios_build_lanes do |options|
    update_project_team(
        path: ENV["PROJECT_IOS"],
        teamid: ENV["TEAM_ID"]
    )

    upgrade_super_old_xcode_project(
        path: ENV["PROJECT_IOS"],
        team_id: ENV["TEAM_ID"]
    )

    match(
        type: options[:match_type],
        readonly: is_ci
    ) # more information: https://codesigning.guide

    disable_automatic_code_signing(
        path: ENV["PROJECT_IOS"],
        team_id: ENV["TEAM_ID"],
        code_sign_identity: options[:code_sign_identity],
        profile_name: "match " + options[:match_profile_type] + " " + ENV["APP_ID"],
        bundle_identifier: ENV["APP_ID"]
    )
  end

  after_all do |lane|
    # This block is called, only if the executed lane was successful

    # slack(
    #   message: "Successfully deployed new App Update."
    # )
  end

  error do |lane, exception|
    # slack(
    #   message: exception.message,
    #   success: false
    # )
  end
end

platform :android do
  before_all do
    ENV["APP_ID"] = CredentialsManager::AppfileConfig.try_fetch_value(:app_identifier)

    puts "Fastlane launched with following environment (Android): "
    puts "Application : "
    puts "\tID : " + ENV["APP_ID"]
    puts "\tName : " + ENV["APP_NAME"]
    puts "\tVersion : " + ENV["APP_VERSION"]
    puts "Andoid Environment :"
    puts "\tApplication ID : " + ENV["APP_ID"]
  end

  desc "Prepare Android platform"
  lane :prepare_platform do
    sh "cd .. && npx ionic cordova platform add android"
  end

  desc "Build Android ionic, generate released apk file"
  lane :build_ionic do
    sh "cd .. && npx ionic cordova build android --release"
  end

  desc "Sign released apk file"
  lane :sign_release do
    sh "cd .. && apksigner sign -ks my-keystore.jks --ks-key-alias upload-keystore --ks-pass env:KS_PASSWORD app-release-unsigned.apk"
  end

  desc "Upload released apk file"
  lane :deploy_release do
    upload_to_play_store(
        package_name: ENV["APP_ID"],
        track: "internal",
        json_key: "certificates/android/api-token.json",
        apk: "platforms/android/app/build/outputs/apk/release/app-release.apk",
        skip_upload_images: true,
        skip_upload_metadata: true,
        skip_upload_screenshots: true
    )
  end

  after_all do |lane|
    # This block is called, only if the executed lane was successful

    # slack(
    #   message: "Successfully deployed new App Update."
    # )
  end

  error do |lane, exception|
    # slack(
    #   message: exception.message,
    #   success: false
    # )
  end
end


# More information about multiple platforms in fastlane: https://github.com/fastlane/fastlane/blob/master/fastlane/docs/Platforms.md
# All available actions: https://docs.fastlane.tools/actions

# fastlane reports which actions are used
# No personal data is recorded. Learn more at https://github.com/fastlane/enhancer
