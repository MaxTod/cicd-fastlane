# MacOS Fastlane configuration

[Fastlane](https://docs.fastlane.tools/) used with [Jenkins](https://jenkins.io/doc/) as part of mobile software factory. It will be used to generate mobile applications in **IPA** and **APK** format, sign these applications and deploy them in the appropriate stores.


## Prerequisites

[Fastlane](https://docs.fastlane.tools/) is a community project that uses [Ruby](https://www.ruby-lang.org/fr/), it is advisable to keep the versions of these tools up-to-date.

### Ruby
Check [ruby version with fastlane](https://docs.fastlane.tools/actions/ruby_version/) (if this one is already installed) and check [ruby environment for a Jenkins CICD](https://docs.fastlane.tools/best-practices/continuous-integration/jenkins/#ruby-environment). If necessary, update the Ruby version as follows (Ruby is installed by default on MacOS systems).

> NOTE : The [mobile CICD install script](https://gitlab.com/MaxTod/macos-setup-ci) makes the necessary checks and installations for Fastlane. 

#### Update Ruby
The whole next step is done in a terminal.

* check Ruby version installed ```ruby -v```
* for Rbenv :
  * check Rbenv version ```rbevn -v```
  * install Rbenv if necessary ```brew install rbenv```
  * init Rbenv ```rbenv init```
  * choose Ruby version with ```echo $(rbenv install -l | grep -v -)``` command or [visit the Ruby website](https://www.ruby-lang.org/en/downloads/)
  * install the chosen version of Ruby ```rbenv install [version]```
  * set the ruby version to use by default ```rbenv global [version]```
  * check that the version of ruby is taken into account ```ruby -v```
* for RVM :
  * check RVM version ```rvm -v```
  * install RVM if necessary ```curl -L https://get.rvm.io | bash -s stable```
  * source the rvm script to start using it directly in the current terminal ```. ~/.rvm/scripts/rvm```
  * choose Ruby version with the ```rvm list known``` command or [visit the Ruby website](https://www.ruby-lang.org/en/downloads/)
  * install the chosen version of Ruby ```rvm install ruby-[version]```
  * set the ruby version to use by default ```rvm --default use [version]```
  * check that the version of ruby is taken into account ```ruby -v```
* if necessary, source the terminal profile  (eg: ```. ~/.bash_profile```)

### Fastlane
It is recommended to install via the [official documentation](https://docs.fastlane.tools/#getting-started). See also the documentation of mobile CICD installation for more information on homebrew and the fastlane facility.
If necessary, install fastlane manually

```gem install fastlane -NV``` or ```brew cask install fastlane```

```export PATH=~/.fastlane/bin:$PATH```

Checkup fastlane and update if necessary (start command with ```bundle exec``` only if installed from ```gem```)

```bundle exec fastlane update_fastlane```

## Usage
This repo suggests a standard configuration for fastlane, to use it, it is necessary to fill the following environment variables, preferably while passing by the environment of Jenkins instance (eg: from build parameters or pipelines):

* application : 
	* **PARAM\_APP\_ID** : application ID
	* **PARAM\_APP\_NAME** : application name
	* **PARAM\_APP\_VERSION** : application version
* for IOS :
	* **PARAM\_APPLE\_ID** : Apple ID used to upload and sign IPA file
	* **PARAM\_TEAM\_NAME** : Development team name
	* **PARAM\_TEAM\_ID** : Development team ID
	* **PARAM\_PROJECT\_IOS** : Name of XCode project directory (in case of a build directly via an XCode project)
	* **PARAM\_WORKSPACE\_IOS** : Name of XCode workspace directory (in case of a build via an XCode workspace)
	* **PARAM\_SCHEME** : Scheme used by XCode workspace (in case of a build via an XCode workspace)
* for Android : 
	* _TODO_

